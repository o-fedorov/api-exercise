"""Management tasks."""
from typing import List

from invoke import Result, UnexpectedExit, task


class _CollectFailures:
    def __init__(self, ctx):
        self._failed: List[Result] = []
        self._ctx = ctx

    def run(self, command: str, **kwargs):
        kwargs.setdefault("warn", True)
        cmd_result: Result = self._ctx.run(command, **kwargs)
        if cmd_result.ok:
            self._ctx.run("echo Ok")
        else:
            self._failed.append(cmd_result)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._failed:
            raise UnexpectedExit(self._failed[0])


@task
def test(ctx):
    """Run tests."""
    # Note: use commandline arguments instead of using `adopts` in `setup.cfg`,
    # since pytest-cov breaks Intellij IDEA debugger.
    ctx.run("poetry run pytest --cov=app -vv .")


@task  # noqa: WPS213 "Found too many expressions"
def check(ctx):  # noqa: WPS213 (yes, it should be placed twice)
    """Run static checks."""
    with _CollectFailures(ctx) as new_ctx:
        print("Checking Black formatting.")
        new_ctx.run("poetry run black . --check")

        print("Checking the style.")
        new_ctx.run("poetry run flake8")

        print("Checking the libraries.")
        new_ctx.run("poetry run safety check")

        print("Checking the terraform files.")
        with ctx.cd("./infrastructure/"):
            new_ctx.run("terraform init")
            new_ctx.run("terraform fmt -check -recursive")
            new_ctx.run("terraform validate")


@task
def fmt(ctx):
    """Apply automatic code formatting."""
    with _CollectFailures(ctx) as new_ctx:
        new_ctx.run("poetry run isort -rc .")
        new_ctx.run("poetry run black .")
        new_ctx.run("terraform fmt -recursive ./infrastructure/")


@task
def run_dev(ctx):
    """Run the app in development mode."""
    ctx.run("poetry run uvicorn app.main:app --reload")
