provider "kubernetes" {
  version = "~> 1.10"
}

provider "random" {
  version = "~> 2.2"
}

locals {
  app               = "api-exercise"
  tier              = "api"
  root_password_key = "root_password"
  migration_files   = "${fileset(abspath("${path.module}/../migrations/"), "*.*")}"
}

resource "kubernetes_namespace" "api-exercise" {
  metadata {
    labels = {
      app = local.app
    }

    name = "api-exercise"
  }
}

resource "random_password" "mysql_root_password" {
  length = 16
}


resource "kubernetes_secret" "mysql-secret" {
  metadata {
    name      = "mysql-secret"
    namespace = kubernetes_namespace.api-exercise.metadata.0.name
  }

  data = {
    "${local.root_password_key}" = random_password.mysql_root_password.result
  }
}

module "mysql" {
  source            = "./mysql"
  secret_name       = kubernetes_secret.mysql-secret.metadata.0.name
  root_password_key = local.root_password_key
  namespace         = kubernetes_namespace.api-exercise.metadata.0.name
}

resource "kubernetes_deployment" "api-exercise" {
  metadata {
    name      = "api-exercise"
    namespace = kubernetes_namespace.api-exercise.metadata.0.name
    labels = {
      app = local.app
    }
  }

  spec {
    selector {
      match_labels = {
        app  = local.app
        tier = local.tier
      }
    }
    template {
      metadata {
        labels = {
          app  = local.app
          tier = local.tier
        }
      }
      spec {
        container {
          image = var.app_image
          name  = "api-exercise"

          port {
            container_port = 8000
          }

          env {
            name = "MYSQL_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.mysql-secret.metadata.0.name
                key  = local.root_password_key
              }
            }
          }
          env {
            name  = "MYSQL_USER"
            value = "root"
          }
          env {
            name  = "MYSQL_HOST"
            value = module.mysql.host
          }

          liveness_probe {
            http_get {
              path = "/health"
              port = 8000
            }
          }
        }

        init_container {
          name    = "init-db"
          image   = "mysql:5.7"
          command = ["bash", "/var/migrations/apply_migrations.sh"]

          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.mysql-secret.metadata.0.name
                key  = local.root_password_key
              }
            }
          }
          env {
            name  = "MYSQL_USER"
            value = "root"
          }
          env {
            name  = "MYSQL_HOST"
            value = module.mysql.host
          }

          volume_mount {
            mount_path = "/var/migrations"
            name       = "migrations"
            read_only  = true
          }
        }

        volume {
          name = "migrations"
          config_map {
            name = kubernetes_config_map.db-migration.metadata.0.name
          }
        }

      }
    }
  }
}

resource "kubernetes_service" "api-exercise" {
  metadata {
    name      = "api-exercise"
    namespace = kubernetes_namespace.api-exercise.metadata.0.name
  }
  spec {
    selector = {
      app  = local.app
      tier = local.tier
    }
    port {
      port        = 80
      target_port = 8000
    }

    type = "LoadBalancer"
  }
}

// Copy DB migration files to the config map.
// Keep this resource config at the bottom of the file
// since it breaks parsing in IDEA.
resource "kubernetes_config_map" "db-migration" {
  metadata {
    name      = "db-migration"
    namespace = kubernetes_namespace.api-exercise.metadata.0.name
  }
  data = { for f in local.migration_files : f => "${file(abspath("${path.module}/../migrations/${f}"))}" }
}
