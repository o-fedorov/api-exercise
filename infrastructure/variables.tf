variable "app_image" {
  type        = string
  default     = "ofedorov/api-exercise:0.1.0"
  description = "Docker image for the API server."
}
