
output "lb_hostname" {
  value = kubernetes_service.api-exercise.load_balancer_ingress.0.hostname
}

output "lb_ip" {
  value = kubernetes_service.api-exercise.load_balancer_ingress.0.ip
}
