variable "namespace" {
  type        = string
  description = "K8s namespace for MySQL."
}

variable "secret_name" {
  type        = string
  description = "Name of the k8s secret that contain MySQL credentials."
}

variable "root_password_key" {
  type        = string
  description = "Name of the k8s secret key that contain MySQL root password."
}
