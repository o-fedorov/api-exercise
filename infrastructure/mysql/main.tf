locals {
  app  = "api-exercise"
  tier = "mysql"
}


resource "kubernetes_persistent_volume_claim" "mysql-pv-claim" {
  metadata {
    name      = "mysql-pv-claim"
    namespace = var.namespace
    labels = {
      app = local.app
    }
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
  }
}

resource "kubernetes_pod" "api-exercise-mysql" {
  metadata {
    name      = "api-exercise-mysql"
    namespace = var.namespace
    labels = {
      app  = local.app
      tier = local.tier
    }
  }

  spec {
    container {
      image = "mysql:5.7"
      name  = "mysql"

      port {
        container_port = 3306
        name           = "mysql"
      }

      env {
        name = "MYSQL_ROOT_PASSWORD"
        value_from {
          secret_key_ref {
            name = var.secret_name
            key  = var.root_password_key
          }
        }
      }

      volume_mount {
        mount_path = "/var/lib/mysql"
        name       = "mysql-persistent-storage"
      }

      readiness_probe {
        exec {
          command = ["bash", "-c", "set -eu -o pipefail; mysql -h 127.0.0.1 -p$MYSQL_ROOT_PASSWORD -e 'SELECT 1' &> /dev/null"]
        }
        initial_delay_seconds = 5
        period_seconds        = 2
        timeout_seconds       = 1
      }
    }
    volume {
      name = "mysql-persistent-storage"
      persistent_volume_claim {
        claim_name = kubernetes_persistent_volume_claim.mysql-pv-claim.metadata.0.name
      }
    }
  }
}

resource "kubernetes_service" "api-exercise-mysql" {
  metadata {
    name      = "api-exercise-mysql"
    namespace = var.namespace
  }
  spec {
    selector = {
      app  = local.app
      tier = local.tier
    }
    port {
      port = 3306
    }

    cluster_ip = null
  }
}
