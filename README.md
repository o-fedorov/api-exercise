# API-exercise
RESTfull service that provides basic access to Titanic passengers data.

## Deploy the app

### Pre-requisites.
The deployment is created and tested for a
[docker-for-desktop](https://www.docker.com/products/docker-desktop) cluster.
Also it should work for any k8s cluster, some adjustments
may be required (like other load balancer implementation).  

Current instruction assumes that you already have
[kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl) and
[terraform](https://www.terraform.io/downloads.html) CLI.

Also you should already have a kubernetes cluster up and running, and
`kubectl` should be configured to work with the cluster.

**Warning**: Make sure your current `kubectl` configuration points to 
correct context.  To do so, run `kubectl config current-context`.

Use commands `kubectl config get-contexts` and 
`kubectl config use-context <context name>` if you need to switch to
another context (cluster).

### Apply the execution plan.
Terraform configuration files are located in [infrastructure](infrastructure)
directory.  

1\. First `cd` to the directory:
```sh
cd infrastructure/
```

2\. Initialize terraform:
```sh
terraform init
```
 
This will download required plugins.
Normally you perform this action only once, for a fresh configuration or when
a new terraform plugin should be added.

3\. Apply the execution plan.

If you want to review the plan before applying, run following command.
```sh
terraform apply
```
Review the changes and enter `yes` if they look good.

**Note**: The output text is huge, since the contents of
[migrations](migrations) directory is included as a config map.
In real world DB population would be moved to a separate step, and the
data would be placed to a remote storage.

Alternatively, execute the command in a non-interactive way:
```sh
terraform apply -auto-approve
```

After the plan is applied, you may see the IP and hostname of the API server.
Also for `docker-for-desktop` no IP is provided, and `localhost` is used as a host name.

Example output:

```
Apply complete! Resources: 10 added, 0 changed, 0 destroyed.

Outputs:

lb_hostname = localhost
lb_ip =
``` 

Use a value which one is correct to access the Web UI of the application.
On OS X you may use following shortcut command:

```bash
open http://$( terraform output lb_hostname )

# or:
open http://$( terraform output lb_ip )
```

This will open the URL with a default web browser.

4\. Remove the deployment (optional, when it's not needed anymore).

When you're done with the deployment and want to free resources,
simply run `terraform destroy` from `./infrastructure` directory.

**Warning:** When performed, this operation can't be undone.

## For Developers

### Tools.
You need [invoke](http://www.pyinvoke.org/) and 
[poetry](https://python-poetry.org/) to work with the repo.

[invoke](http://www.pyinvoke.org/) can be installed with 
[pipx](https://pipxproject.github.io/pipx/) for python3.7+.
Alternatively run `poetry shell` from the root of the repo to get into
virtual environment where [invoke](http://www.pyinvoke.org/) and all
project dependencies are already installed.

[invoke](http://www.pyinvoke.org/) is used to run some common tasks:

```
inv fmt       # Apply automatic code formatting.
inv check     # Run static checks.
inv test      # Run tests.
inv run-dev   # Run the app in development mode.
```

The tasks may be chained as following: `inv fmt check test`.

Run `inv -l` to see a list of all available tasks.

### Running tests.
Tests require connection to MySQL.
  
The fastest way to get MySQL server is to run a command like following:

```shell script
docker run -d --name=test-mysql -p 3306:3306 -e 'MYSQL_ROOT_PASSWORD=Passw0rd!' mysql:5.7
```
Then populate `.env` file which is to be used by `python-decouple`
(you need to do it only once).

```shell script
echo "MYSQL_USER=root
MYSQL_HOST=127.0.0.1
MYSQL_PASSWORD=Passw0rd!
" > .env
```

Run the tests with `inv test`.  
When done with testing, call `docker kill test-mysql`.

#### Alternative MySQL configuration
If you have the infrastructure
deployed to `docker-for-desktop` as described in the [Deploy the app](#deploy-the-app)
section, perform following steps in the root directory of the repo:

```bash
# Get MySQL root password.
MYSQL_ROOT_PASSWORD=$(kubectl exec -n api-exercise -it api-exercise-mysql -- /bin/bash -c 'echo $MYSQL_ROOT_PASSWORD')

# Populate `.env` file, to be used by `python-decouple`.
echo "MYSQL_USER=root
MYSQL_HOST=127.0.0.1
MYSQL_PASSWORD=$MYSQL_ROOT_PASSWORD
" > .env
```

Now start MySQL port forwarding to be able to connect to it from your
machine.  Open a new terminal window and execute following command:

```
kubectl port-forward pod/api-exercise-mysql 3306
```

Now running `inv test` should pass.

### (Re)building a container.
There is a GitLab CI job configured to run container build.  Simply add
a Git tag `release-<version>`, where `<version>` is a
release or pre-release semantic version like `0.1.0`,  `0.1.1dev0`, `0.2.1rc3`
and so on.  It will trigger building and pushing an image 
`ofedorov/api-exercise` of specified version.
 
You can build your own container image manually with commands provided below.
Just make sure your kubernetes cluster will be able pull this image.

```sh
docker build -t <IMAGE NAME AND A TAG> .
docker push <IMAGE NAME AND A TAG>

# For example:
docker build -t ofedorov/api-exercise:0.1.1dev0 .
docker push ofedorov/api-exercise:0.1.1dev0

```

If you change the image name or the tag (version), pass it to `terraform`
when applying the plan:

```sh
terraform apply -var 'app_image=<IMAGE NAME AND A TAG>'

# For example:
terraform apply -var 'app_image=ofedorov/api-exercise:0.1.1dev0'
```

Alternatively, create a file `infrasturcture/terraform.tfvars`
with related contents:

```
app_image = <IMAGE NAME AND A TAG>
```

Also you may want to edit the default image name in 
[infrastructure/variables.tf](infrastructure/variables.tf) file 
(`app_image` variable) to make the changes permanent.
