"""Request and response models."""
from enum import Enum
from uuid import UUID

from pydantic import BaseModel, Field


class Sex(str, Enum):
    """Person sex values."""

    male = "male"
    female = "female"
    other = "other"


class PersonData(BaseModel):
    """Data used to create a person."""

    survived: bool
    passenger_class: int = Field(..., alias="passengerClass")
    name: str = Field(..., max_length=255)  # noqa: WPS432 (it's not a "magic" number)
    sex: Sex = Field(..., max_length=10)
    age: int
    siblings_or_spouses_aboard: int = Field(..., alias="siblingsOrSpousesAboard")
    parents_or_children_aboard: int = Field(..., alias="parentsOrChildrenAboard")
    fare: float


class Person(PersonData):
    """Outgoing person representation."""

    uuid: UUID

    class Config:
        """Model config class.

        See https://pydantic-docs.helpmanual.io/usage/model_config/ for details.
        """

        allow_population_by_field_name = True

    @classmethod
    def from_db(cls, model_data: dict) -> "Person":
        """Build a person from a DB entry."""
        model_data = model_data.copy()
        uuid = model_data.pop("uuid")
        return cls(uuid=UUID(bytes=uuid), **model_data)


class Message(BaseModel):
    """Generic response message."""

    message: str
