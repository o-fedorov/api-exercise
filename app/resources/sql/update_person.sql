-- :name update_person :affected
UPDATE people SET
  survived=:survived,
  passenger_class=:passenger_class,
  `name`=:name,
  sex=:sex,
  age=:age,
  siblings_or_spouses_aboard=:siblings_or_spouses_aboard,
  parents_or_children_aboard=:parents_or_children_aboard,
  fare=:fare
WHERE uuid=:uuid;
