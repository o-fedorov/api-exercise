-- :name add_person :insert
INSERT INTO people (
  uuid,
  survived,
  passenger_class,
  `name`,
  sex,
  age,
  siblings_or_spouses_aboard,
  parents_or_children_aboard,
  fare
) VALUES (
  :uuid,
  :survived,
  :passenger_class,
  :name,
  :sex,
  :age,
  :siblings_or_spouses_aboard,
  :parents_or_children_aboard,
  :fare
);
