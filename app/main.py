"""Main application entry point."""
import uvicorn
from fastapi import FastAPI
from starlette.responses import PlainTextResponse, RedirectResponse

from app.db import queries
from app.routers import router
from app.settings import get_connection_string

app = FastAPI()
app.include_router(router, prefix="/people", tags=["people"])

queries.connect(get_connection_string())


@app.get("/", include_in_schema=False)
def home():
    """Redirect to the OpenAPI docs."""
    return RedirectResponse(url="/docs")


@app.get("/health", include_in_schema=False)
def health():
    """Redirect to the OpenAPI docs."""
    return PlainTextResponse("Ok")


if __name__ == "__main__":
    # Debug-only configuration
    uvicorn.run(app)
