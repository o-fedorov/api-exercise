"""Application configuration."""
from decouple import config
from sqlalchemy.engine.url import URL

DB_USER = config("MYSQL_USER")
DB_PASSWORD = config("MYSQL_PASSWORD")
DB_HOST = config("MYSQL_HOST")
DB_DATABASE = config("MYSQL_DATABASE", "titanic")


def get_connection_string(
    *, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, database=DB_DATABASE
):
    """Build a connection string for using with PugSQL or SQLAlchemy."""
    return str(URL("mysql+pymysql", username=user, password=password, host=host, database=database))
