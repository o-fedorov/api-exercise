"""Common place for SQL queries."""
from pathlib import Path

import pugsql

_RESOURCES = Path(__file__).parent / "resources"

queries = pugsql.module(_RESOURCES / "sql")
