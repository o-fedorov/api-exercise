"""Path operations related to people API."""
from typing import List
from uuid import UUID, uuid1

from fastapi import APIRouter
from starlette.responses import JSONResponse
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND

from .db import queries
from .models import Message, Person, PersonData

router = APIRouter()


@router.get("", status_code=HTTP_200_OK, response_model=List[Person])
async def list_people():
    """Get a list of all people."""
    return (Person.from_db(v) for v in queries.fetch_people())


@router.post("", status_code=HTTP_200_OK, response_model=Person)
async def add_people(person_data: PersonData):
    """Add a person to the database."""
    uuid = uuid1()
    queries.add_person(uuid=uuid.bytes, **person_data.dict())
    return {"uuid": uuid, **person_data.dict()}


@router.get(
    "/{uuid}",
    status_code=HTTP_200_OK,
    response_model=Person,
    responses={HTTP_404_NOT_FOUND: {"model": Message}},
)
async def get_person(uuid: UUID):
    """Get information about one person."""
    resp = queries.fetch_person(uuid=uuid.bytes)
    if resp:
        return Person.from_db(resp)

    return JSONResponse(status_code=HTTP_404_NOT_FOUND, content={"message": "Item not found"})


@router.put(
    "/{uuid}",
    status_code=HTTP_200_OK,
    response_model=Person,
    responses={HTTP_404_NOT_FOUND: {"model": Message}},
)
async def update_person(uuid: UUID, person: PersonData):
    """Update information about one person."""
    updated = queries.update_person(uuid=uuid.bytes, **person.dict())
    if updated:
        return {"uuid": uuid, **person.dict()}

    return JSONResponse(status_code=HTTP_404_NOT_FOUND, content={"message": "Item not found"})


@router.delete(
    "/{uuid}",
    status_code=HTTP_200_OK,
    response_model=Message,
    responses={HTTP_404_NOT_FOUND: {"model": Message}},
)
async def delete_person(uuid: UUID):
    """Delete this person."""
    deleted = queries.delete_person(uuid=uuid.bytes)
    if deleted:
        message = "Deleted"
        status = HTTP_200_OK
    else:
        message = "Item not found"
        status = HTTP_404_NOT_FOUND

    return JSONResponse(status_code=status, content={"message": message})
