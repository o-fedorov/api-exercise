FROM python:3.7 as builder

# DISABLE pip cache and version check, increase the timeout.
ENV PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

ENV POETRY_VERSION=1.0.3

# Use poetry for dependencies management.
RUN pip install "poetry==$POETRY_VERSION"

# Copy only poetry-specific files to collect the dependencies.
WORKDIR /app
COPY poetry.lock pyproject.toml /app/

# Install dependencies in current directory
RUN poetry config virtualenvs.in-project true \
  && poetry install --no-dev --no-interaction --no-ansi


FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

# Nonroot user for running the application
RUN groupadd -g 999 app && \
    useradd -r -u 999 -g app app


# Custom home for easier interactions inside container
ENV HOME "/app"

# Use the Python from virtualenv
ENV PATH "/app/.venv/bin:${PATH}"
ENV PYTHONPATH "/app/.venv/lib/python3.7/site-packages:${PATH}"

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PORT=8000

# Copy the app code to the place expected by the runner
# and make app user own everything in the directory.
# Do it in two steps for layers cache optimization.
COPY --from=builder /app .
RUN chown -R app:app /app

COPY ./app /app/app/
RUN chown -R app:app /app/app

# Switch user
USER app
