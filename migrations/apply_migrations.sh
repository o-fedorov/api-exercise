#!/usr/bin/env bash

# exit when any command fails, when some variables are not set or when a pipe command has failed.
set -eu -o pipefail

DIR=$( dirname "$0" )

echo "Initializing the DB."
mysql -h$MYSQL_HOST -p$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE IF NOT EXISTS titanic;"

for migration in $DIR/*.sql; do
    echo "Applying '$migration'."
    mysql -h$MYSQL_HOST -p$MYSQL_ROOT_PASSWORD titanic < $migration
done
