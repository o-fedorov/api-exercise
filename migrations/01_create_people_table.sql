CREATE TABLE IF NOT EXISTS people (
  uuid BINARY(16) PRIMARY KEY,
  survived TINYINT(1),
  passenger_class INT,
  `name` VARCHAR(255),
  sex VARCHAR(10),
  age INT,
  siblings_or_spouses_aboard INT,
  parents_or_children_aboard INT,
  fare FLOAT
);