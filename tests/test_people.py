"""Tests for `/people` path operations."""
from unittest.mock import ANY

from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND

from .conftest import ROOT, assert_json_response


def test_get_people(client):
    with ROOT.joinpath("titanic.csv").open() as titanic_file:
        expected_rows_num = len(titanic_file.readlines()) - 1

    response = client.get("/people")
    assert_json_response(response, ANY)

    people = response.json()
    assert len(people) == expected_rows_num

    assert {
        "survived": True,
        "passengerClass": 2,
        "name": "Miss. Simonne Marie Anne Andree Laroche",
        "sex": "female",
        "age": 3,
        "siblingsOrSpousesAboard": 1,
        "parentsOrChildrenAboard": 2,
        "fare": 41.5792,
        "uuid": "ff642ac2-4a55-11ea-b615-acde48001122",
    } in people


def test_get_prson(client):
    response = client.get("/people/ff642ac2-4a55-11ea-b615-acde48001122")
    assert_json_response(
        response,
        {
            "survived": True,
            "passengerClass": 2,
            "name": "Miss. Simonne Marie Anne Andree Laroche",
            "sex": "female",
            "age": 3,
            "siblingsOrSpousesAboard": 1,
            "parentsOrChildrenAboard": 2,
            "fare": 41.5792,
            "uuid": "ff642ac2-4a55-11ea-b615-acde48001122",
        },
    )


def test_get_people_empty_db(client, clear_table):
    response = client.get("/people")
    assert_json_response(response, [])


def test_get_non_existing_person(client):
    response = client.get("/people/00000000-0000-0000-C000-000000000046")
    assert_json_response(response, {"message": "Item not found"}, HTTP_404_NOT_FOUND)


def test_update_non_existing_person(client):
    response = client.put(
        "/people/00000000-0000-0000-C000-000000000046",
        json={
            "survived": True,
            "passengerClass": 1,
            "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
            "sex": "female",
            "age": 38,
            "siblingsOrSpousesAboard": 1,
            "parentsOrChildrenAboard": 0,
            "fare": 71.2833,
        },
    )
    assert_json_response(response, {"message": "Item not found"}, HTTP_404_NOT_FOUND)


def test_delete_non_existing_person(client):
    response = client.delete("/people/00000000-0000-0000-C000-000000000046")
    assert_json_response(response, {"message": "Item not found"}, HTTP_404_NOT_FOUND)


def test_post_person(client):
    person = {
        "survived": True,
        "passengerClass": 1,
        "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
        "sex": "female",
        "age": 38,
        "siblingsOrSpousesAboard": 1,
        "parentsOrChildrenAboard": 0,
        "fare": 71.2833,
    }

    response = client.post("/people", json=person)
    assert_json_response(response, {"uuid": ANY, **person})


def test_get_people_person_created(client, clear_table):
    response = client.post(
        "/people",
        json={
            "survived": True,
            "passengerClass": 1,
            "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
            "sex": "female",
            "age": 38,
            "siblingsOrSpousesAboard": 1,
            "parentsOrChildrenAboard": 0,
            "fare": 71.2833,
        },
    )
    assert_json_response(response, ANY, HTTP_200_OK)
    person = response.json()

    response = client.get("/people")
    assert_json_response(response, [person])


def test_update_existing_person(client, clear_table):
    response = client.post(
        "/people",
        json={
            "survived": True,
            "passengerClass": 1,
            "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
            "sex": "female",
            "age": 38,
            "siblingsOrSpousesAboard": 1,
            "parentsOrChildrenAboard": 0,
            "fare": 71.2833,
        },
    )
    assert_json_response(response, ANY, HTTP_200_OK)
    person = response.json()

    uuid = person.pop("uuid")
    person["fare"] = 42.3
    person["age"] = 22

    response = client.put(f"/people/{uuid}", json=person)
    assert_json_response(response, {"uuid": uuid, **person})

    response = client.get("/people")
    assert_json_response(response, [{"uuid": uuid, **person}])


def test_delete_existing_person(client, clear_table):
    response = client.post(
        "/people",
        json={
            "survived": True,
            "passengerClass": 1,
            "name": "Mrs. John Bradley (Florence Briggs Thayer) Cumings",
            "sex": "female",
            "age": 38,
            "siblingsOrSpousesAboard": 1,
            "parentsOrChildrenAboard": 0,
            "fare": 71.2833,
        },
    )
    assert_json_response(response, ANY, HTTP_200_OK)
    person = response.json()

    uuid = person.pop("uuid")

    response = client.delete(f"/people/{uuid}", json=person)
    assert_json_response(response, {"message": "Deleted"})

    response = client.get("/people")
    assert_json_response(response, [])
