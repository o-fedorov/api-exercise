"""Common test code and fixtures."""
import contextlib
from pathlib import Path
from typing import Union

import pymysql
import pytest
from requests import Response
from starlette.testclient import TestClient

from app import db, settings
from app.main import app

ROOT: Path = Path(__file__).parent.parent
MIGRATIONS: Path = ROOT / "migrations"
TEST_DB = "test_people"


def assert_json_response(
    response: Response, expected_json: Union[dict, list], expected_code: int = 200
):
    """Make sure the response is an expected JSON with an expected code."""
    assert response.status_code == expected_code, response.content
    assert response.json() == expected_json


@pytest.fixture
def client() -> TestClient:
    """Web application client fixture."""
    return TestClient(app)


@pytest.fixture(autouse=True)
def db_connection() -> pymysql.Connection:
    """Create testing DB and table, clean up after a test."""
    connection = pymysql.connect(
        host=settings.DB_HOST,
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor,
    )

    with contextlib.closing(connection):
        _setup_db(connection)
        db.queries.connect(settings.get_connection_string(database=TEST_DB))

        yield connection

        _teardown_db(connection)


def _setup_db(connection):
    with connection.cursor() as initialization_cursor:
        initialization_cursor.execute(f"DROP DATABASE IF EXISTS {TEST_DB};")
        initialization_cursor.execute(f"CREATE DATABASE IF NOT EXISTS {TEST_DB};")
        connection.select_db(TEST_DB)

        for migration in sorted(MIGRATIONS.glob("*.sql")):
            initialization_cursor.execute(migration.read_text())
    connection.commit()


def _teardown_db(connection):
    with connection.cursor() as drop_cursor:
        drop_cursor.execute(f"DROP DATABASE IF EXISTS {TEST_DB};")
    connection.commit()


@pytest.fixture
def clear_table(db_connection):
    """Ensure there is no data in `people` table."""
    with db_connection.cursor() as cursor:
        cursor.execute(f"DELETE FROM people;")
    db_connection.commit()
