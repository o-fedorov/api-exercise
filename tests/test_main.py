"""Main module tests."""
from starlette.status import HTTP_200_OK


def test_root_redirect(client):
    response = client.get("/")
    assert response.status_code == HTTP_200_OK
    assert response.url == "http://testserver/docs"


def test_health(client):
    response = client.get("/health")
    assert response.status_code == HTTP_200_OK
    assert response.text == "Ok"
